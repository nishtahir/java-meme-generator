package com.nishtahir;

import com.sun.istack.internal.NotNull;

import javax.imageio.ImageIO;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.font.FontRenderContext;
import java.awt.font.LineBreakMeasurer;
import java.awt.font.TextAttribute;
import java.awt.font.TextLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.AttributedCharacterIterator;
import java.text.AttributedString;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * Small library to generate memes.
 */
public class MemeGenerator {

    /**
     * Default stroke width.
     */
    private static final int STROKE_WIDTH = 2;

    /**
     * Nice padding around text so it's not
     * touching the borders.
     */
    private static final int TEXT_PADDING = 5;

    /**
     * Base text size.
     */
    private static final int BASE_FONT_SIZE = 40;

    /**
     * Width of the source image.
     */
    private int width;

    /**
     * Height of the source image.
     */
    private int height;

    /**
     *
     */
    @NotNull
    private Graphics2D g2d;

    /**
     *
     */
    @NotNull
    private static Font font;

    static {
        GraphicsEnvironment gEnv = GraphicsEnvironment
                .getLocalGraphicsEnvironment();
        List<String> availableFonts = Arrays.asList(
                gEnv.getAvailableFontFamilyNames());
        if (availableFonts.contains("Impact")) {
            font = new Font("Impact", Font.PLAIN, BASE_FONT_SIZE);
        } else {
            InputStream resource = MemeGenerator.class.getClassLoader()
                    .getResourceAsStream("bebas_neue.otf");
            try {
                font = Font.createFont(Font.PLAIN, resource);
            } catch (FontFormatException | IOException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * Draws meme text on Image using Impact font.
     *
     * @param topText    Text to draw on top.
     * @param bottomText Text to draw at the bottom.
     * @param image      Image to draw on.
     * @return Meme
     */
    protected BufferedImage drawTextOnImage(@NotNull final String topText,
                                            @NotNull final String bottomText,
                                            @NotNull final Image image) {
        width = image.getWidth(null);
        height = image.getHeight(null);

        BufferedImage renderedImage = new BufferedImage(
                width, height, BufferedImage.TYPE_INT_RGB);
        g2d = renderedImage.createGraphics();

        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.drawImage(image, 0, 0, null);

        drawStringAsTopText(topText.toUpperCase());
        drawStringAsBottomText(bottomText.toUpperCase());

        g2d.dispose();
        return renderedImage;
    }


    /**
     * @param topText text to appear at the top of the image.
     */
    protected void drawStringAsTopText(@NotNull final String topText) {
        if (topText.isEmpty()) {
            return;
        }
        Font topFont = font.deriveFont((float)
                getScaledFontSize(topText.length()));

        AttributedString attributedString = new AttributedString(topText);
        attributedString.addAttribute(TextAttribute.FONT, topFont);
        attributedString.addAttribute(TextAttribute.FOREGROUND, Color.WHITE);

        AttributedCharacterIterator characterIterator = attributedString
                .getIterator();
        FontRenderContext fontRenderContext = g2d.getFontRenderContext();
        LineBreakMeasurer measurer = new LineBreakMeasurer(characterIterator,
                fontRenderContext);

        int x = TEXT_PADDING;
        int y = 0;

        while (measurer.getPosition() < characterIterator.getEndIndex()) {
            int insetWidth = width - x;
            TextLayout textLayout = measurer.nextLayout(insetWidth);
            float drawPosX = (float) x
                    + (insetWidth - textLayout.getAdvance()) / 2;
            y += textLayout.getAscent();

            TextLayoutUtils.drawWithOutline(textLayout,
                    g2d, drawPosX, y, getStrokeWidth());
            y += textLayout.getDescent() + textLayout.getLeading();
        }
    }

    /**
     * @param bottomText text to appear at the bottom of the image.
     */
    protected void drawStringAsBottomText(@NotNull final String bottomText) {
        if (bottomText.isEmpty()) {
            return;
        }
        Font topFont = font.deriveFont((float)
                getScaledFontSize(bottomText.length()));

        AttributedString attributedString = new AttributedString(bottomText);
        attributedString.addAttribute(TextAttribute.FONT, topFont);
        attributedString.addAttribute(TextAttribute.FOREGROUND, Color.WHITE);

        AttributedCharacterIterator characterIterator = attributedString
                .getIterator();
        FontRenderContext fontRenderContext = g2d.getFontRenderContext();
        LineBreakMeasurer measurer = new LineBreakMeasurer(characterIterator,
                fontRenderContext);

        int x = 0;
        int y = height - TEXT_PADDING;
        int insetWidth = width - TEXT_PADDING;

        //This is here simply because I want to reverse the order that
        //the text is laid out.
        List<TextLayout> layoutList = new ArrayList<>();
        while (measurer.getPosition() < characterIterator.getEndIndex()) {
            layoutList.add(measurer.nextLayout(insetWidth));
        }

        for (int i = layoutList.size() - 1; i >= 0; i--) {
            TextLayout textLayout = layoutList.get(i);
            float drawPosX = (float) x
                    + (insetWidth - textLayout.getAdvance()) / 2;
            y -= textLayout.getDescent();
            TextLayoutUtils.drawWithOutline(textLayout, g2d,
                    drawPosX, y, getStrokeWidth());
            y -= textLayout.getAscent() + textLayout.getLeading();
        }
    }

    /**
     * @param url        Path to image file.
     * @param output     Where the meme drops.
     * @param topText    text to go on top.
     * @param bottomText text to go on the bottom.
     * @return Meme file. null on error.
     */
    public static File generateMeme(@NotNull final URL url,
                                    @NotNull final File output,
                                    @NotNull final String topText,
                                    @NotNull final String bottomText) {
        try {
            File result;
            if (output == null) {
                result = File.createTempFile(
                        UUID.randomUUID().toString(), "jpg");
            } else {
                result = output;
            }

            Image image = ImageIO.read(url);
            BufferedImage bufferedImage = new MemeGenerator()
                    .drawTextOnImage(topText, bottomText, image);
            ImageIO.write(bufferedImage, "jpg", result);

            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @param url        Path to image file.
     * @param topText    text to go on top.
     * @param bottomText text to go on the bottom.
     * @return Meme file. Null on error.
     */
    public static File generateMeme(@NotNull final URL url, @NotNull final String topText,
                                    @NotNull final String bottomText) {
        return generateMeme(url, null, topText, bottomText);
    }

    /**
     * @param image      Image file to parse.
     * @param output     Where to drop the meme.
     * @param topText    text to go on top.
     * @param bottomText text to go on the bottom.
     * @return Meme file. Null on failure
     */
    public static File generateMeme(@NotNull final File image, @NotNull final File output,
                                    @NotNull final String topText,
                                    @NotNull final String bottomText) {
        try {
            return generateMeme(image.toURI().toURL(),
                    output, topText, bottomText);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @param image      Image file to parse.
     * @param topText    text to go on top.
     * @param bottomText text to go on the bottom.
     * @return Meme file. Null on failure.
     */
    public static File generateMeme(@NotNull final File image, @NotNull final String topText,
                                    @NotNull final String bottomText) {
        return generateMeme(image, null, topText, bottomText);
    }

    /**
     * @param path       path to image.
     * @param outputPath output location of meme.
     * @param topText    text to appear at the top of the image.
     * @param bottomText text to appear at the bottom of the image.
     * @return Reference to meme-ified image. Null on error.
     */
    public static File generateMeme(@NotNull final String path,
                                    @NotNull final String outputPath,
                                    @NotNull final String topText,
                                    @NotNull final String bottomText) {
        return generateMeme(new File(path), new File(outputPath),
                topText, bottomText);
    }

    /**
     * @return Stroke width for meme text.
     */
    protected static int getStrokeWidth() {
        return STROKE_WIDTH;
    }

    /**
     * I decided to scale based on char count because the text layout
     * is derived based on the font size. This means that we're forced
     * to estimate before hand.
     *
     * @param charLength Length of chars in text.
     * @return Font size scaled to text length.
     */
    @SuppressWarnings("checkstyle:magicnumber")
    protected static double getScaledFontSize(final int charLength) {
        double scale;
        //CHECKSTYLE.OFF
        if (charLength < 10) {
            scale = 1.0;
        } else if (charLength < 24) {
            scale = 0.9;
        } else if (charLength < 48) {
            scale = 0.8;
        } else {
            scale = 0.7;
        }
        //CHECKSTYLE.ON
        return BASE_FONT_SIZE * scale;
    }

}
