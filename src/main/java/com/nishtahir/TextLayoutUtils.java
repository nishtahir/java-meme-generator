package com.nishtahir;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;

/**
 * Static utility class to help with
 * drawing textlayout components.
 */
public final class TextLayoutUtils {

    /**
     * Prevent instantiation.
     */
    private TextLayoutUtils() {

    }

    /**
     * Draws a text layout with a stroke.
     *
     * @param textLayout  Layout to draw.
     * @param graphics2D  Graphics to draw with.
     * @param x           x axis position to begin drawing.
     * @param y           y axis position to begin drawing.
     * @param strokeWidth Width of stroke or outline.
     */
    public static void drawWithOutline(final TextLayout textLayout,
                                       final Graphics2D graphics2D,
                                       final float x, final float y,
                                       final int strokeWidth) {

        AffineTransform transform = new AffineTransform();
        transform.setToTranslation(x, y);
        Shape shape = textLayout.getOutline(transform);
        graphics2D.setColor(Color.BLACK);
        graphics2D.setStroke(new BasicStroke(strokeWidth));
        graphics2D.draw(shape);
        textLayout.draw(graphics2D, x, y);
    }

}
