package com.nishtahir;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;

import static org.junit.Assert.assertTrue;

public class MemeGeneratorTest {

    private File input = new File("src/test/resources/test.jpg");
    private File output = new File("src/test/resources/output.jpg");

    @Before
    public void setUp() {
        //noinspection ResultOfMethodCallIgnored
        output.delete();
    }

    @After
    public void tearDown() {
        //noinspection ResultOfMethodCallIgnored
        output.delete();
    }

    @Test(expected = NullPointerException.class)
    public void generateMeme_WithInvalidPath_ThrowsException() throws Exception {
        MemeGenerator.generateMeme("", "", "", "");
    }

    @Test(expected = NullPointerException.class)
    public void generateMeme_WithInvalidFile_ThrowsException() throws Exception {
        MemeGenerator.generateMeme(new File("src/test/resources/dummy.txt"), new File(""), "", "");
    }

    @Test
    public void generateMeme_WithEmptyStrings_DoesNotCrash() {
        MemeGenerator.generateMeme(input, output, "", "");
        assertTrue(output.exists());
    }

    @Test
    public void generateMeme_WithValidFile_WorksCorrectly() throws Exception {
        MemeGenerator.generateMeme(input, output, "Updates memegen with scalable font", "& Java memegen actually works!");
        assertTrue(output.exists());
    }

}